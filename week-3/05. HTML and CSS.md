Every web page in the world, including this one, is stored as a series of statements using HyperText Markup Language (HTML) and Cascading Style Sheets (CSS).  These are represented in a way which browsers know how to read, and can decide how to display it.

There is a distinction between HTML and CSS: HTML is responsible for displaying the _content_ of the Web page, whereas CSS is responsible for styling it.  The important thing here, is that the HTML should make semantic sense - if there are no styles, you should be able to still understand the text which is on the page.

## HTML

HTML is displayed as a series of _tags_, or _elements_, and _attributes_.  Some examples of how these might look is as follows: &lt;body&gt;. &lt;p&gt;, &lt;a href="https://www.soton.ac.uk"&gt;, &lt;h1&gt;, &lt;div&gt;

Those were mostly elements, defining the body of the text, a paragraph, a link to another page, a top level heading, and a section of the page.  The `href` is an _attribute: it is not so much part of the content but rather points to a different URL which, if clicked on, will open in the browser.

Each element you see will be "closed", usually by writing a closing tag starting with `</`, and any content inside the two tags will be considered a part of that tag.  Imagine the code:
 
    <!DOCTYPE html>
    <html>
        <head>
            <title>This is the page's title</title>
        </head>
        <body>
            <h1>This is a heading</h1>
            <p>
                Lorem ipsum dolur sit amet, 
                <a href="http://example.com">Click me!</a>
            </p>
        </body>
    </html>

In it, we can see the following:

* A `DOCTYPE`, which simply specifies that the following text is going to fit the specification of a HTML document
* A `<html>` tag, which indicates the start of the HTML
* A `<head>` tag, inside the HTML, which normally includes metadata about the document, in this case the title of the page.
* a `<body>` defined, and within it contains a top level heading (`<h1>`), a paragraph (`<p>`), and a link
* An _anchor_ tag `<a>`, which means thhat it links somewhere (i.e., the bit inside the `href` attribute).  The text inside the two tags `Click me!` is what will be displayed.  We can see the whole thing laid out as follows:

 
<!DOCTYPE html>
<html>
    <head>
        <title>This is the page's title</title>
    </head>
    <body>
        <h1>This is a heading</h1>
        <p>
            Lorem ipsum dolur sit amet, 
            <a href="http://example.com">Click me!</a>
        </p>
    </body>
</html>


There is more to HTML of course, but you can get away with knowing very little more than that to be able to write Web pages!

When thinking about our example of travelling between cities from week 1, we could envision a piece of HTML which displayed the data in a table, with one row representing a single city, and each corresponding cell representing the distance to each other city as follows:

    <table>
        <tr>
            <th> - </th><th>Aberystwyth</th><th>Brighton</th><Edinburgh</th>
        </tr>
        <tr>
            <td>Aberystwyth</td><td>0</td><td>252</td><td>500</td>
        </tr>
    </table>

It looks as follows:

| - | Aberystwyth | Brighton | Edinburgh |
|---|-------------|----------|-----------|
| Aberystwyth | 0 | 252 | 500|
| Brighton | 252 | 0 | 600 |

Notice here that we represent a table as an opening set of tags, and inside we have `<tr>`, `<th>`, and `<td>` tags.  These stand for table row (tr), table header (th) and table data (td).  These all help to ensure that the information presented makes semantic sense.

It is quite challenging to extract this information with a computer, and is better off attempting to parse CSV or JSON files (see next sections)

This shows us how HTML is represented, and from this our browsers can understand what the content is all about, and how it can be represented on the screen.  There is another important part of this though, and this is the way that it represents the information.  A key part of HTML is that it is supposed to make semantic sense.  We should not include a paragraph unless it would be sensible to do so, and a table should only be used for tabular data, rather than to move content around for display purposes.  

As a result of this principle, CSS was born to deal with the display side...

## CSS

In a Web page, CSS is responsible for dealing with the display of the page, and has a completely separate syntax.  This separation of responsibilities makes writing Web pages easier to manage, and for computers to understand.

CSS sets out styles using a series of key/value pairs, enclosed in a definition of the location of an element or series of elements.  For example, to make all the text of the document red, with Arial font, we would use the following:

    body{
        color: red;
        font-family: Arial;
    }
        
## Further reading
Obviously, this barely scratches the surface of HTML and CSS.  There are complete references available for creating Web pages, which is beyond the scope of this course.  For now, the point we're trying to get across is that they are both a way of representing information, in a format which a Web browser can understand.

If you are interested in learning HTML and CSS in more detail, there is a complete reference available at w3schools, and a course available on CodeAcademy.