# Comparison of algorithms

\[This is a video\]

In the last video, we found that there are situations where we need to be a bit smarter about the algorithms we choose for performing tasks.  In this video, we provide some intuition about how to assess the performance of an algorithm, and other concepts we need to think about when defining an algorithm: tractability, running time, memory requirements, and the application scenario.

In the next section, we will introduce _Big O_ notation, which adds formalism for the worst case scenario of running an algorithm.

Slide outline: https://docs.google.com/presentation/d/1Mmbwc8Saz6ilu14uJjSnGpDDbAbbd8DCho3GuSyccd4/edit
