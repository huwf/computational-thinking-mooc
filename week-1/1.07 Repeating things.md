## Repeating Things

Often, we wish to have our algorithm do things more than once.  Computers are good at repeating things, so it makes sense for us to say "_do this thing x times_", or "_for each item in this list do something_".

In the last section, we looked at how an algorithm makes decisions.  Now suppose we were going further than Winchester, but wished to make that decision at a series of cities all the way along our route.

Imagine we want to go from Southampton, Winchester, Basingstoke, and Farnham, and want to make the same decision each time, we could make the same decision each time:

    if amount_of_cars_before_winchester > 10000:
        use the back streets
    else:
        use the motorway
    if amount_of_cars_before_basingstoke > 10000:
        use the back streets
    else:
        use the motorway
    if amount_of_cars_before_farnham > 10000:
        use the back streets
    else:
        use the motorway

Even with just four cities, the code starts to look pretty cumbersome!  Luckily, we can include a shortcut in our code, known as a loop.  So now we could write it like this:

    for towns in list_of_towns:
        if amount_of_cars_before_town > 10000:
            use the back streets
        else:
            use the motorway
        
Here, we imagine a list of towns, and we work through each one of those, and do the same thing: Decide whether to take the motorway or the backstreets.

We could also do this whilst a condition is holding.  Maybe we are on the motorway and each time we are wondering "should we leave at this exit", we could define our thought process like this:


    while on_motorway:
        if next_junction_leads_to_destination:
            leave at this junction
            break out of the loop
        else:
            stay on motorway

This represents a repeating situation which we keep looping until we reach a certain condition, and then `break` out of the loop.

We will go into some examples later in the week where we practice these concepts with Python code.
