import csv
import math
import collections
import random
import datetime
RANDOM_THRESHOLD = 0.5
random.seed(42)


class Node(object):

    def __init__(self, label='Node'):
        self.label = label
        self.edges = []

    def __repr__(self):
        return '<Node %s>' % self.label

    def add_edge(self, edge, add_reverse=False):
        self.edges.append(edge)
        # print(self.edges)
        if add_reverse:
            reverse = edge.reverse()
            edge.to_node.edges.append(reverse)
            return {'edge': edge, 'reverse': reverse}

        return edge


class Edge(object):

    def __init__(self, from_node, to_node, distance=1, directed=0):
        self.from_node = from_node
        self.to_node = to_node
        self.distance = distance
        self.directed = directed

    def __repr__(self):
        return '<Edge %s -> %s = %d>' % (self.from_node.label, self.to_node.label, self.distance)

    def reverse(self, distance=None):
        if distance is None:
            distance = self.distance
        e = Edge(from_node=self.to_node, to_node=self.from_node, distance=distance)
        if self.directed:
            e.directed = True
            e.distance = self.directed
        return e


class Graph(object):
    operations = 0

    def __init__(self, nodes=[], edges=[], filepath= ''):
        # print('nodes:', nodes)
        self.nodes = {n.label: n for n in nodes}
        # print(self.nodes)

        self.edges = edges
        # self.connect_nodes()
        self.distances = {}
        self.path = {}
        if filepath:
            self.filepath = filepath
            self.cities = self.get_cities()
            self.nodes = {n.label: n for n in self.create_city_nodes()}
            self.get_distances_between_cities()


    def get_node_by_label(self, label):
        if label in self.nodes:
            return self.nodes[label]
        return None


    def connect_nodes(self, label_a, label_b, distance=1):
        if random.random() < RANDOM_THRESHOLD:
            return
        # print('label_a: %s, label_b: %s Distance: %d' % (label_a, label_b, distance))
        node_a = self.get_node_by_label(label_a)
        node_b = self.get_node_by_label(label_b)
        e = Edge(node_a, node_b, int(distance))
        self.edges.append(e)
        # Add reverse edge, and append to self.edges
        e = node_a.add_edge(e, False)
        reverse = e.reverse()
        e.to_node.edges.append(reverse)
        self.edges.append(reverse)

    def dijkstra(self, source):
        node_objects = [self.get_node_by_label(n) for n in self.nodes]
        self.distances = {n: 0 if n == source else math.inf for n in node_objects}
        self.path = {n: None for n in node_objects}
        visited = set()
        queue = collections.deque([source])# + node_objects)

        while len(queue) > 0:
            node = queue.popleft()
            # if path[node] is not None:
            #     continue
            if node in visited:
                continue
            visited.add(node)

            # print('current_weight', current_weight)
            for e in node.edges:
                # current_weight = 0 if self.distances[node] == math.inf else self.distances[node]
                if e.to_node not in visited:
                    queue.append(e.to_node)
                current_weight = self.distances[node]
                # print('e: ', e, e.distance)
                distance = current_weight + e.distance
                if distance < self.distances[e.to_node]:
                    self.distances[e.to_node] = distance
                    self.path[e.to_node] = e

        # print(self.path)
        # print(self.distances)

    def get_shortest_path(self, source, dest):
        if dest not in self.path:
            return
        shortest_path = []
        edge = self.path[dest]
        while True:
            shortest_path.insert(0, edge)
            if dest == source:
                break
            if edge.from_node == source:
                return [edge]

            edge = self.path[edge.from_node]
            dest = edge.from_node

        return shortest_path

    def get_cities(self):
        cities = []
        with open(self.filepath) as f:
            for c in f.readlines():
                cities.append(c.split(',')[0])
        # print(cities)
        return cities

    def create_city_nodes(self):
        return [Node(label=c) for c in self.cities]

    def get_distances_between_cities(self):
        cities = self.cities

        with open(self.filepath) as f:
            reader = csv.reader(f)
            for row in reader:
                for i in range(len(row)):
                    if row[i] == '':
                        continue
                    # City name, or city -> city
                    try:
                        if i > 0:
                            j = int(row[i])
                    except:
                        pass
                    if i == 0 or int(row[i]) == 0:
                        continue
                    # Optimisation: Since we have reversed the edges, we don't need to process half of them
                    if i > cities.index(row[0]):
                        continue
                    city = row[0]
                    dest = cities[i-1]
                    self.connect_nodes(city, dest, distance=int(row[i]))

    @staticmethod
    def generate_random_graph(path, size):
        t = datetime.datetime.now()
        stringy = ''
        with open(path, 'w') as f:
            for i in range(size):
                for j in range(size):
                    if j == 0:
                        # Node label
                        stringy += '%s,' % str(i + 1)

                    if i == j or (i == 0 and j == 1):
                        stringy += '%d' % 0
                        break
                    stringy += '%d,' % random.randint(0, 1000)
                f.write('%s\n' % stringy)
                stringy = ''

        g = Graph(filepath=path, edges=[])
        print('Time taken to generate random graph with %d nodes and %d edges:' % (len(g.nodes), len(g.edges)), datetime.datetime.now() - t)
        return g

    @staticmethod
    def generate_graph_from_file(filepath):
        t = datetime.datetime.now()
        g = Graph(filepath=filepath, edges=[])
        print('Generated graph of %d nodes and %d edges in...' % (len(g.nodes), len(g.edges)), datetime.datetime.now() - t)
        return g

    @staticmethod
    def display_time_taken_for_shortest_path(graph, source=None, dest=None):
        if not source and not dest:
            source = graph.get_node_by_label('1')
            dest = graph.get_node_by_label('2')
        t = datetime.datetime.now()
        graph.dijkstra(source)
        print(graph.get_shortest_path(source, dest))
        print('Time taken for shortest path (%d nodes, %d edges):' % (len(graph.nodes), len(graph.edges)), datetime.datetime.now() - t)





if __name__ == '__main__':
    # na = Node('A')
    # nb = Node('B')
    # nc = Node('C')
    # nd = Node('D')
    #
    # e1 = Edge(na, nb)
    # e2 = Edge(na, nc)
    # e3 = Edge(nb, nc)
    # e4 = Edge(nb, nd)
    # e5 = Edge(nc, nd, distance=0)
    #
    # na.add_edge(e1)
    # na.add_edge(e2)
    # nb.add_edge(e3)
    # nb.add_edge(e4)
    # nc.add_edge(e5)
    #
    # nodes = [na, nb, nc, nd]
    #
    # # g = Graph(nodes, [])
    # source = na
    print('Generating graphs.....')
    g4 = Graph.generate_random_graph('test2048.txt', 4)
    g8 = Graph.generate_random_graph('test2048.txt', 8)
    graph1 = Graph.generate_graph_from_file(filepath='uk_cities.txt')
    g32 = Graph.generate_random_graph('test2048.txt', 32)
    g64 = Graph.generate_random_graph('test2048.txt', 64)
    graph2 = Graph.generate_graph_from_file(filepath='us_cities.txt')
    graph3 = Graph.generate_graph_from_file(filepath='us_cities_312.txt')
    g512 = Graph.generate_random_graph('test2048.txt', 512)
    g1024 = Graph.generate_random_graph('test2048.txt', 1024)
    g2048 = Graph.generate_random_graph('test2048.txt', 2048)
    # g4096 = Graph.generate_random_graph('test2048.txt', 4096)

    Graph.display_time_taken_for_shortest_path(g4)
    Graph.display_time_taken_for_shortest_path(g8)
    source = graph1.get_node_by_label('Aberystwyth')
    dest = graph1.get_node_by_label('Stratford')
    Graph.display_time_taken_for_shortest_path(graph1, source, dest)

    Graph.display_time_taken_for_shortest_path(g32)
    Graph.display_time_taken_for_shortest_path(g64)

    source = graph2.get_node_by_label('Victoria')
    dest = graph2.get_node_by_label('Winston-Salem')
    Graph.display_time_taken_for_shortest_path(graph2, source, dest)

    source = graph3.get_node_by_label('Raleigh')
    dest = graph3.get_node_by_label('Troy')
    Graph.display_time_taken_for_shortest_path(graph3, source, dest)

    Graph.display_time_taken_for_shortest_path(g512)
    Graph.display_time_taken_for_shortest_path(g1024)
    Graph.display_time_taken_for_shortest_path(g2048)
    # Graph.display_time_taken_for_shortest_path(g4096)
























