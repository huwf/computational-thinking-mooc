{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Making Decisions\n",
    "\n",
    "Earlier in the week, we covered making decisions in algorithms. In Python, the main way of doing this is with the \"if\" statement.\n",
    "\n",
    "If statements allow you to conditionally execute code. They are given a value and if that value evaluates to True, the code contained in the if statement is executed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "This prints if the value is true!\n"
     ]
    }
   ],
   "source": [
    "if True:\n",
    "    #Code to execute if True\n",
    "    print(\"This prints if the value is true!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we want to do something when the value isn't true, we can extend the if statement with an `else`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "This prints if the value is true!\n"
     ]
    }
   ],
   "source": [
    "value_to_check = True\n",
    "\n",
    "if value_to_check:\n",
    "    #Code to execute if True\n",
    "    print(\"This prints if the value is true!\")\n",
    "else:\n",
    "    #Code to execute if False\n",
    "    print(\"This prints if the value is false!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "value_to_check = False\n",
    "\n",
    "if not value_to_check:\n",
    "    print(\"This prints if the value is false!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Whitespace and Blocks\n",
    "\n",
    "You might have noticed in the section above that the code contained within the 'if statement' was indented. That's because python is a whitespace sensitive language. In particular, it's sensitive to identation.\n",
    "\n",
    "Whitespace consists of things like spaces, tabs and newlines, that can't be seen, but add space to the code. Python reads these, and considers them a part of the language's <a href=\"https://en.wikipedia.org/wiki/Syntax_(programming_languages)\">syntax</a>.\n",
    "\n",
    "The code that is executed as part of an if statement is contained in the block below it. A block is a series of equally indented lines. \n",
    "\n",
    "For example, the statement below prints:\n",
    "\n",
    "* \"Some value\" if `some_value` is `True`. \n",
    "* \"Other Value\" if `some_value` is `True` **and** `other_value` is `True`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Some value\n"
     ]
    }
   ],
   "source": [
    "some_value = True\n",
    "other_value = False\n",
    "\n",
    "if some_value:\n",
    "    #Block 1, part of the first if statement\n",
    "    print(\"Some value\")\n",
    "    \n",
    "    if other_value:\n",
    "        #Block 2, part of the first AND second if statement.\n",
    "        print(\"Other value\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comparison Operators\n",
    "\n",
    "Comparison operators allow you to compare 2 values, and return either `True` or `False`. Some of the most commonly used ones are:\n",
    "\n",
    "* *`a < b`* True if `a` is strictly less than `b`\n",
    "* *`a > b`* True if `a` is strictly greater than `b`\n",
    "* *`a <= b`* True if `a` is less than or equal to `b`\n",
    "* *`a >= b`* True if `a` is greater than or equal to `b`\n",
    "* *`a == b`* True if `a` is exactly equal to `b`\n",
    "* *`a != b`* True if `a` is not equal to `b`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1 is less than 2\n",
      "3 is greater than 2\n",
      "2 is less than or equal to 2\n",
      "2 is greater than or equal to 2\n",
      "Yes is equal to Yes\n",
      "No does not equal Yes\n"
     ]
    }
   ],
   "source": [
    "if 1 < 2:\n",
    "    print(\"1 is less than 2\")\n",
    "    \n",
    "if 3 > 2:\n",
    "    print(\"3 is greater than 2\")\n",
    "    \n",
    "if 2 <= 2:\n",
    "    print(\"2 is less than or equal to 2\")\n",
    "    \n",
    "if 2 >= 2:\n",
    "    print(\"2 is greater than or equal to 2\")\n",
    "\n",
    "if \"Yes\" == \"Yes\":\n",
    "    print(\"Yes is equal to Yes\")\n",
    "    \n",
    "if \"No\" != \"Yes\":\n",
    "    print(\"No does not equal Yes\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Logical Operators\n",
    "\n",
    "Before we start playing around with if statements though, we should extend our mighty decision making powers with a few new operators: `and` and `or`. \n",
    "\n",
    "* *`and`* evaluates to True if *both* of its arguments are true, else it evaluates to False\n",
    "* *`or`* evaluates to True if *either* of its arguments are true, else it evaluates to False\n",
    "* *`not`* evaluates to True if it's argument is False, and False if its argument is True (not is unary - it only takes one argument)\n",
    "\n",
    "Let's see an example. Imagine we were writing a system to calculate ticket fares for trains to London. We could do something like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The ticket price is: £100\n"
     ]
    }
   ],
   "source": [
    "train_goes_to_london = True\n",
    "train_is_busy = True\n",
    "\n",
    "if train_goes_to_london and train_is_busy:\n",
    "    ticket_price = 100\n",
    "\n",
    "if train_goes_to_london and not train_is_busy:\n",
    "    ticket_price = 50\n",
    "\n",
    "if not train_goes_to_london:\n",
    "    ticket_price = 5\n",
    "\n",
    "print(\"The ticket price is: £\" + str(ticket_price))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That was all very verbose though - we can definitely make it more concise! Enter the `elif`, short for 'else if'. It allows you to execute code if the previous if statement evaluated to False and its argument evaluates to true."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The ticket price is: £100\n"
     ]
    }
   ],
   "source": [
    "train_goes_to_london = True\n",
    "train_is_busy = True\n",
    "\n",
    "if train_goes_to_london and train_is_busy:\n",
    "    ticket_price = 100\n",
    "elif train_goes_to_london:\n",
    "    ticket_price = 50\n",
    "else:\n",
    "    ticket_price = 5\n",
    "    \n",
    "print(\"The ticket price is: £\" + str(ticket_price))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Have a play around with the booleans, and make sure you understand exactly how the `elif` works before continuing!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Practice: Dogs, planes and decisions\n",
    "\n",
    "Let's get a bit more practice using if statements. Continuing with our transport theme, we're going to write some code decide if dogs are allowed on a plane.\n",
    "\n",
    "Dogs are allowed on a plane if:\n",
    "\n",
    "* They are a small dog (less than 50cm long)\n",
    "* They are well behaved\n",
    "\n",
    "However, there's also an extra fee:\n",
    "\n",
    "* Dogs normally cost £10 to take on a plane\n",
    "* If a dog is *wet* or *sheds hair*, it costs *£20* extra to cover the cleaning costs. \n",
    "\n",
    "For the dog below, determine if it's allowed on the plane, and how much it costs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The dog is allowed on the plane: True | Should be: True\n",
      "The cost of the dog is: 30 | Should be: 30\n"
     ]
    }
   ],
   "source": [
    "length_of_dog = 30\n",
    "dog_well_behaved = \"Yes\"\n",
    "dog_is_wet = False\n",
    "dog_sheds_hair = True\n",
    "\n",
    "#Your code goes below this line\n",
    "#------------------------------\n",
    "\n",
    "#Do not change below this line\n",
    "#------------------------------\n",
    "\n",
    "print(\"The dog is allowed on the plane:\", dog_allowed_on_plane, \"| Should be:\", \"True\")\n",
    "print(\"The cost of the dog is:\", cost_of_dog, \"| Should be:\", \"30\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Loops, lists and dicts\n",
    "\n",
    "## Loops\n",
    "\n",
    "The final part of this exercise is to consider loops, and the data structures that can be looped over.\n",
    "\n",
    "Loops are the main way of performing repetition in Python, and look like this:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "for x in y:\n",
    "    z\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This means, **for every item x that is in the sequence y, do z**. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for num in range(0, 10):\n",
    "    print(num)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`range(start, limit, step)` is a new function, that creates a sequence of numbers between *start* and *limit*, exclusive. *step* is an optional parameter - It determines how much larger the next number in the sequence will be. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This gives us all the numbers that, when starting at the number 10 and adding 3 each time, are less than 20.\n",
    "for num in range(10, 20, 3):\n",
    "    print(num)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Lists\n",
    "\n",
    "Being able to repeat something X times is great - but even better is being able to do something once for every item in a list of items. \n",
    "\n",
    "Lists in Python are a new type. They contain a number of items which can be access by their position in the list (starting at 0)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#A list of strings.\n",
    "list_of_fruits = [\"apple\", \"orange\", \"banana\"]\n",
    "\n",
    "#The first fruit, the apple, is at position 0.\n",
    "print(list_of_fruits[0])\n",
    "\n",
    "#Similarly, the banana is at 2.\n",
    "print(list_of_fruits[2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see here a new notation - square brackets. These can be used to access a specific item a list... or a dictionary, but we'll get to that later.\n",
    "\n",
    "We can also loop over the items in a list, much in the same way we did the sequence of numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list_of_fruits = [\"apple\", \"orange\", \"banana\"]\n",
    "\n",
    "for item in list_of_fruits:\n",
    "    print(item)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dictionaries\n",
    "\n",
    "Dictionaries are a type of data structure that stores key-value pairs. Each key has exactly one value associated with it, and this key can be any of a variety of types. However, strings are the most common. \n",
    "\n",
    "The syntax for creating and accessing dictionaries is similar to that of arrays, and is demonstrated below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Create a dictionary, where each key is a city name, and each value is their distance from London by car.\n",
    "distance_to_city = {\"London\": 0, \"Glasgow\": 100, \"Southampton\": 20, \"Leeds\": 50, \"Manchester\": 40}\n",
    "\n",
    "#Retrieving the value associated with \"Southampton\"\n",
    "distance_to_southampton = distance_to_city[\"Southampton\"]\n",
    "\n",
    "print(\"Southampton is\", distance_to_southampton, \"miles away from London\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dictionaries can't be looped over in exactly the same way as lists - they need interpreting as a sequence. You can do this in three ways:\n",
    "\n",
    "* View all of the keys as a sequence\n",
    "* View all of the values as a sequence\n",
    "* View all of the keys and values as a sequence of pair tuples (don't worry about these!)\n",
    "\n",
    "For now, we'll just consider keys, which can be looped over like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#To get a sequence of keys from a dictionary, use: [name_of_dictionary].keys()\n",
    "for key in distance_to_city.keys():\n",
    "    print(\"Key:\", key)\n",
    "    #Access the value of the key as we would normally\n",
    "    print(\"Value:\", distance_to_city[key], \"\\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the previous example, we also showed you have you access the values - the same way you would any other time! Notice that we can also use the contents of a variaable as a key.\n",
    "\n",
    "Finally: congratulations, that's pretty much all you need to know about dictionaries right now!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Practice: The Cheapest Flights\n",
    "\n",
    "There's many places someone might fly from London for a holiday. But how much does visiting each one cost? You know now enough to figure it out!\n",
    "\n",
    "Below, you've been given a cost to get to a London airport, a dictionary with a set of destinations and the cost to fly to each.\n",
    "\n",
    "You've been given as input a list of places a particular person would like to visit.\n",
    "\n",
    "Your job to find them the cheapest place to visit!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The cheapest city to visit is Your answer\n"
     ]
    }
   ],
   "source": [
    "list_of_places_to_visit = [\"Glasgow\", \"Lisbon\", \"Freiburg\", \"Paris\", \"New York\"]\n",
    "\n",
    "cost_of_flight = {\n",
    "    \"Glasgow\": 30,\n",
    "    \"Madrid\": 45,\n",
    "    \"Moscow\": 90,\n",
    "    \"New Deli\": 110,\n",
    "    \"Lisbon\": 80,\n",
    "    \"Hong Kong\": 350,\n",
    "    \"Munich\": 95,\n",
    "    \"Lyon\": 35,\n",
    "    \"Edinburgh\": 20,\n",
    "    \"Freiburg\": 60,\n",
    "    \"Leeds\": 60,\n",
    "    \"Kyoto\": 600,\n",
    "    \"Sydney\": 500,\n",
    "    \"Paris\": 40,\n",
    "    \"Rome\": 40,\n",
    "    \"Bucharest\": 70,\n",
    "    \"New York\": 300,\n",
    "    \"Amsterdam\": 30,\n",
    "    \"Oslo\": 70,\n",
    "    \"Southampton\": 200,\n",
    "    \"Cardiff\": 55\n",
    "}\n",
    "\n",
    "#Your code goes here\n",
    "#-------------------\n",
    "\n",
    "cheapest_city_name = \"Your answer\"\n",
    "\n",
    "#Do not change after this line\n",
    "#-----------------------------\n",
    "\n",
    "print(\"The cheapest city to visit is\", cheapest_city_name)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3.0
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}