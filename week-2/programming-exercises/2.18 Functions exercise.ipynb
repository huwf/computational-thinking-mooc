{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions\n",
    "\n",
    "You have already seen some functions in week 1, here we will go through them more thoroughly.  To recap, at its simplest, a function is an abstraction of some functionality, which allows you to repeat it many times.  There are many different names for them, which are often used interchangeably, such as function, method, or procedure.  In Python, the most commonly used name is _functions_, or _methods_ when they are part of a class.\n",
    "\n",
    "A function we used before is the `print` function, which is part of the core Python library.  To run it, you simply type `print`, and the value of what you want outputted inside the parentheses.  So to print \"Hello, world!\", we simply run as follows below:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Hello, world!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we will make our own functions which we can call in our own code.  The syntax for writing a function is:\n",
    "\n",
    "\tdef function_name(param1, param_2):\n",
    "\t\t# Everything in the function is in this code block\n",
    "\n",
    "The `def` stands for _define_, because you are defining your own function.  Let's see an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_hello():\n",
    "    print('Hello!')\n",
    "    \n",
    "print_hello()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you run this code, you will see that it prints the output `hello`.  The function can be broken down as follows:\n",
    "\n",
    "1. We define the name of the function to be `print_hello`.  The empty parentheses after the function indicate that there are no parameters in the definition.\n",
    "2. The next line is indented, to signify it is a part of the function, and so when the function is called, this code will run\n",
    "3. The function is called, and the code inside is run.\n",
    "\n",
    "## Returning values\n",
    "\n",
    "The function above only performed a task, and did not give an output.  When we want a function to provide an output, we `return` the value at the end of the function.  This can be used like any other variable.\n",
    "\n",
    "Consider the code below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_square_of_2():\n",
    "    return 2 * 2\n",
    "\n",
    "output = get_square_of_2()\n",
    "print(output)\n",
    "print(output + 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this code, we performed a calculation on a value, and returned the value.  We did not print it, but rather it gave us an integer to be assigned to our variable `output`, and operated on in normal ways.  You can see that we had to call `print` on it to get the output printed.  You can see the difference in these two functions as well by using the `type` function, which shows the type of the output:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(print_hello())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(get_square_of_2())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most functions you use should have an output, even if they are simply performing a task such as running a series of other functions.  The reason for this, is that for the purposes of abstraction, knowing what you _get_ for a particular function definition is far more useful than simply seeing it done.  Going back to our cake example, if you were to have a function `add_flour(mix)`, you would have the mix returned with the new ingredient added, so that you can continue to work with it.\n",
    "\n",
    "Aside from the usefulness, by including an output, it allows a function to report a particular status of what has happened.  For example, a function might usually return a value of `0` if everything went right, but return a value of `-1` if something goes wrong.  Other functions can check for the value, and react accordingly depending on the program logic."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parameters and arguments\n",
    "\n",
    "These functions so far have been simple, only performing one line of code, and not allowing any sort of dynamic result depending on input.  This is changed by the use of _parameters_ and _arguments_.  These are often (incorrecty) used interchangeably, since they refer to the same thing.  The difference is that a parameter refers to values in the _definition_ of a function, whereas an argument is used in the implementation of a function.  We will see this more clearly in the example below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_output(output):\n",
    "    print(output)\n",
    "    \n",
    "print_output('Hello, world!')\n",
    "print_output('Goodbye!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the example above, we defined a more general version of the `print_hello` function, which included a parameter called `output`.  The function printed the value of the parameter `output`.\n",
    "\n",
    "On lines 4 and 5, we see then the implementation of the function, which passes in the _arguments_ `'Hello, world!'` and `Goodbye!`, where these are both printed to screen.\n",
    "\n",
    "We are not limited to one parameter either, we can include several, which are separated by a comma.  An example of a similar function might be as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_outputs(output1, output2, output3='Optional text'):\n",
    "    str_output = output1 + output2 + output3\n",
    "    print(str_output)\n",
    "\n",
    "print_outputs('Hello ', 'world', '!')\n",
    "print_outputs('Hello, ', 'world\"')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example, we see two things.  Firstly, we see on line 1 that we have two parameters, as well as a parameter with a value preassigned.  Here, if we do not include a value for the third argument, it will take on the value assigned to it by default.  You can see the two different outputs on the two calls of `print_outputs` in the cell.\n",
    "\n",
    "## Exercises\n",
    "\n",
    "1. In the cell below, create a function `square_number`, which takes a single parameter and returns the square of this value.\n",
    "2. Create another function `cube_number` which takes a single parameter and returns the cube of this value.\n",
    "3. Call both of these functions, and assign the outputs into variables.  Add the values of the two previous functions, and print them to screen. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary\n",
    "\n",
    "In this exercise, you have seen how to define and use functions, and how these could be used to abstract away "
   ]
  }
 ],
 "metadata": {},
 "nbformat": 4,
 "nbformat_minor": 0
}