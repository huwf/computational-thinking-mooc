{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Classes and objects\n",
    "\n",
    "## Introduction\n",
    "\n",
    "As we saw previously, functions are a means of abstracting a problem.  They can be considered to be a black box: you know what to put in, and you know what comes out but are unaware of what happens inside.  Another abstraction which is commonly used is that of classes and objects.  This is known as the _object oriented_ programming paradigm.\n",
    "\n",
    "It works by representing elements of the problem domain within the code.  You don't need to represent everything about it, because many of the details are superfluous to what we are trying to accomplish.  Consider the example from last week of routes between a city.  You might wish to represent a city in your code.  What attributes do you think it might have?\n",
    "\n",
    "\tName\n",
    "\tSize\n",
    "\tPopulation\n",
    "\tNeighbouring cities\n",
    "\tMain language spoken\n",
    "\tAll the street names\n",
    "\tCompanies running the public transport\n",
    "\t....\n",
    "\n",
    "All these might be important information if you wish to live there, but for our code, we don't need to worry about them.  In fact, all we really need is `name`, and `neighbouring_cities`, everything else is unnecessary detail.\n",
    "\n",
    "With this in mind, consider the important distinction below of the difference between a class and an object.\n",
    "\n",
    "1. A _class_ is a representation of a thing.  This is a specification where everything is defined.  In this example, `City` is a class\n",
    "2. An object is an instance of a class.  You can create many of these, which apply the specification.  For example, we might create an object `aberystwyth`, which would keep the details of Aberystwyth.\n",
    "\n",
    "Consider the code below, and run it to see the output:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "class City:\n",
    "    name = ''\n",
    "    neighbouring_cities = []\n",
    "\n",
    "    def __init__(self, name, neighbouring_cities):\n",
    "        self.name = name\n",
    "        self.neighbouring_cities = neighbouring_cities\n",
    "        \n",
    "aberystwyth = City('Aberystwyth', ['Hereford', 'Swansea', 'Birmingham'])\n",
    "print(aberystwyth.name, aberystwyth.neighbouring_cities)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a few things happening in this snippet of code, which we will go through line-by-line.\n",
    "\n",
    "\n",
    "* In line 1, we define a class called `City`.  As with conditionals, loops, and functions, everything after this is indented once, and will be regarded as part of this class\n",
    "* In lines 2-3 we include two attributes of the class, `name` and `neighbouring_cities`, set to be an empty string and an empty list.\n",
    "* In lines 5-7, we have a function called `__init__`, with three parameters: `self`, `name`, and `neighbouring_cities`.  This is a special function which runs when an _instance_ (object) of the class is created. The first `self` represents the fact that this method is part of the class.  We use this later to refer to other attributes inside the same class.  The `name` and `neighbouring_cities` parameters are passed like a normal function, and they assign values to the `name` and `neighbouring_cities` based on these parameters.\n",
    "* In line 9 we create an instance of the class called `aberystwyth`, and pass it a name and some neighbouring cities, then we print it on the next line.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "In the cell below, create a class called `Town`.  This should be the same as the `City` class, except it should include an extra Boolean attribute called `has_cathedral` which you should set to false (historically, this is how cities and towns are distinguished in the UK)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inheritance\n",
    "\n",
    "If you completed the exercise, you will have noticed that the code of `City` and `Town` are almost exactly the same. To save having to continually copy the same definitions, the concept of _inheritance_ is used when dealing with classes.  A class can inherit from another, which means that it takes all the attributes and functions from the _parent_ class, and then adds specialisation.  To represent this, consider the example below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "class Place(object):\n",
    "    name = ''\n",
    "    neighbouring_places = []\n",
    "    # Most places are not cities, so we will assume they don't have cathedrals\n",
    "    has_cathedral = False\n",
    "    \n",
    "    def __init__(self, name, neighbouring_places):\n",
    "        self.name = name\n",
    "        self.neighbouring_cities = neighbouring_places\n",
    "    \n",
    "\n",
    "class City(Place):\n",
    "    # This specialises the behaviour of the Place class\n",
    "    has_cathedral = True\n",
    "\n",
    "\n",
    "class Town(Place):\n",
    "    def __init__(self, name, neighbouring_places):\n",
    "        super().__init__(name, neighbouring_places)\n",
    "        print('This is a town')\n",
    "        \n",
    "aberystwyth = Town('Aberystwyth', ['Swansea', 'Hereford', 'Birmingham'])\n",
    "swansea = City('Swansea', [])\n",
    "\n",
    "print('Aberystwyth', vars(aberystwyth))\n",
    "print('Swansea', vars(swansea))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this previous snippet, we made a more abstract representation of a `Place`, which inherited from `object`.  This is the usual way to define classes in Python: every class inherits from the most basic thing, an object. The reasons for this are historical but essentially is needed to keep code compatbile between Python 2 and Python 3.  \n",
    "\n",
    "The `City` class inherits from `Place`, so will contain exactly the same attributes, except for the `has_cathedral` value, which is set to `True`.\n",
    "\n",
    "The `Town` city doesn't change any of the values, but does change the functionality of the constructor method.  The `super()` method calls the function from the parent class first, and then we include our own functionality for the sub class: It prints a line to indicate that we are in a town rather than a city.\n",
    "\n",
    "You can see the way this has changed the values from the two objects created and printed below"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "Add another subclass to `Place` called `Village`.  Add an extra value `small_population` with a value of `True`, and add a line inside the constructor to print `\"This is a village\"`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Methods\n",
    "\n",
    "As well as the special `__init__` method, you can add your own methods to a class.  These can acces all the members of the class and manipulate them. You can call a class method by using the dot notation, as in, `object_name.function_name()`.  In the code below, we will add a method here to remove a city from a class below, and then call it.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "class Place(object):\n",
    "    name = ''\n",
    "    neighbouring_places = []\n",
    "    # Most places are not cities, so we will assume they don't have cathedrals\n",
    "    has_cathedral = False\n",
    "    \n",
    "    def __init__(self, name, neighbouring_places):\n",
    "        self.name = name\n",
    "        self.neighbouring_cities = neighbouring_places\n",
    "    \n",
    "    def remove_place_from_neighbours(self, place_name):\n",
    "        # Find the index of the place to remove\n",
    "        index = self.neighbouring_places.indez(place_name)\n",
    "        # Set the neighbouring places list to remove that\n",
    "        self.neighbouring_places = self.neighbouring_places.remove(index)\n",
    "        print('Neighbour ', place_name, 'removed')\n",
    "        \n",
    "cardiff = City('Cardiff', ['Swansea', 'Bristol'])\n",
    "print(vars(cardiff))\n",
    "cardiff.remove_place_from_neighbours('Swansea')\n",
    "print(vars(cardiff))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "Add a method `add_neighbour` to the `Place` class.  This should modify the value of the `neighbouring_places` attribute of the class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Further work\n",
    "\n",
    "This concludes our introduction to classes and objects.  If you are feeling keen, the code used to create the nodes and edges of the network graphs in week 1 is below.  Try and understand what it's doing (it should hopefully be well commented!) and see if you can change it, or use it to make your own network graph."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "class Node(object):\n",
    "\n",
    "    def __init__(self, label='Node'):\n",
    "        self.label = label\n",
    "        self.edges = []\n",
    "\n",
    "    def __repr__(self):\n",
    "        return '<Node %s>' % self.label\n",
    "\n",
    "    def add_edge(self, edge, add_reverse=False):\n",
    "        self.edges.append(edge)\n",
    "        # print(self.edges)\n",
    "        if add_reverse:\n",
    "            reverse = edge.reverse()\n",
    "            edge.to_node.edges.append(reverse)\n",
    "            return {'edge': edge, 'reverse': reverse}\n",
    "\n",
    "        return edge\n",
    "\n",
    "\n",
    "class Edge(object):\n",
    "\n",
    "    def __init__(self, from_node, to_node, distance=1, directed=0):\n",
    "        self.from_node = from_node\n",
    "        self.to_node = to_node\n",
    "        self.distance = distance\n",
    "        self.directed = directed\n",
    "\n",
    "    def __repr__(self):\n",
    "        return '<Edge %s -> %s = %d>' % (self.from_node.label, self.to_node.label, self.distance)\n",
    "\n",
    "    def reverse(self, distance=None):\n",
    "        if distance is None:\n",
    "            distance = self.distance\n",
    "        e = Edge(from_node=self.to_node, to_node=self.from_node, distance=distance)\n",
    "        if self.directed:\n",
    "            e.directed = True\n",
    "            e.distance = self.directed\n",
    "        return e\n",
    "\n",
    "\n",
    "class Graph(object):\n",
    "    operations = 0\n",
    "\n",
    "    def __init__(self, nodes=[], edges=[]):\n",
    "        # print('nodes:', nodes)\n",
    "        self.nodes = {n.label: n for n in nodes}\n",
    "        # print(self.nodes)\n",
    "\n",
    "        self.edges = edges\n",
    "        # self.connect_nodes()\n",
    "        self.distances = {}\n",
    "        self.path = {}\n",
    "\n",
    "    def get_node_by_label(self, label):\n",
    "        if label in self.nodes:\n",
    "            return self.nodes[label]\n",
    "        return None\n",
    "\n",
    "    def connect_nodes(self, label_a, label_b, distance=1):\n",
    "\n",
    "        # print('label_a: %s, label_b: %s Distance: %d' % (label_a, label_b, distance))\n",
    "        node_a = self.get_node_by_label(label_a)\n",
    "        node_b = self.get_node_by_label(label_b)\n",
    "        e = Edge(node_a, node_b, int(distance))\n",
    "        self.edges.append(e)\n",
    "        # Add reverse edge, and append to self.edges\n",
    "        e = node_a.add_edge(e, False)\n",
    "        reverse = e.reverse()\n",
    "        e.to_node.edges.append(reverse)\n",
    "        self.edges.append(reverse)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [default]",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
