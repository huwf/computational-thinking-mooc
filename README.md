# Computational thinking MOOC

A place to write out the content (in markdown) and exercises for the computational thinking MOOC.  There is a lot of content
  in the MOOC, but the idea is that the students can see only the first step in a week, and ignore the rest if they so 
  choose.  

The plan is to use [Jupyter notebooks](http://jupyter.org) as a REPL and IDE for Python based exercises.
You can install Jupyter with Anaconda 