import flask
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return '<!DOCTYPE html><html><head><title>Hello, world!</title></head><body><h1>Hello, world!</h1>'


@app.route('/cities')
def cities():
    return '<!DOCTYPE html><html><head><title>These are the cities</title></head><body><h1>These are the cities</h1>'


@app.route('/add', methods=['GET', 'POST'])
def add_city():
    if flask.request.method == 'GET':
        return '<html><body><h1>GET Method</h1><p>Click to post</p><form  action="/add" method="POST"><input type="submit" /></form></body></html>'
    else:
        return '<html><body><h1>Posted successfully!</h1></body></html>'


@app.route('/user/<username>')
def get_user(username):
    return ('<h1>Hello, %s</h1>' % username)


if __name__ == '__main__':
    app.run(debug=True)