import flask
from flask import Flask
import sqlite3
app = Flask(__name__)


def _get_cities_data():
    query = 'SELECT city from cities ORDER BY city ASC'
    conn = sqlite3.connect('cities.db')
    cur = conn.cursor()
    cur.execute(query)
    return cur.fetchall()
    # return_list = []
    # for r in cur.fetchall():
    #     return_list.append(r[0])
    # return ['Aberystwyth', 'Brighton', 'Edinburgh']


def _get_city_details(name):
    query = 'SELECT * from cities WHERE name=?'
    conn = sqlite3.connect('cities.db')
    cur = conn.cursor()
    cur.execute(query, name)
    return cur.fetchone()

    # return '''
    #     <h2>Welcome to %s</h2>
    #     <dl>
    #       <dt>Population</dt>
    #       <dd>4567</dd>
    #       <dt>Weather</dt>
    #       <dd>Cold</dd>
    #       <dt>Neighbouring cities</dt>
    #       <dd>City A, City B, ....</dd>
    #     </dl>
    #
    # ''' % name

@app.route('/')
def hello_world():
    return '<!DOCTYPE html><html><head><title>Hello, world!</title></head><body><h1>Hello, world!</h1>'


@app.route('/cities')
def cities():
    cities = _get_cities_data()
    cities_list = '<ul>'
    for c in cities:
        cities_list = cities_list + '<li>%s</li>' % c
    cities_list = cities_list + '</ul>'

    head = '<!DOCTYPE html><html><head><title>These are the cities</title></head>'
    body = '<body><h1>These are the cities</h1>' + cities_list + '</body></html>'
    return head + body


@app.route('/city/<name>')
def city(name):
    display = _get_city_details(name)
    head =  '<!DOCTYPE html><html><head><title>These are the cities</title></head>'
    body = '<body><h1>%s</h1>' % name + display + '</body></html>'
    return head + body



@app.route('/add', methods=['GET', 'POST'])
def add_city():
    if flask.request.method == 'GET':
        return '<html><body><h1>GET Method</h1><p>Click to post</p><form  action="/add" method="POST"><input type="submit" /></form></body></html>'
    else:
        return '<html><body><h1>Posted successfully!</h1></body></html>'


@app.route('/user/<username>')
def get_user(username):
    return ('<h1>Hello, %s</h1>' % username)


if __name__ == '__main__':
    app.run(debug=True)