from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return '<!DOCTYPE html><html><head><title>Hello, world!</title></head><body><h1>Hello, world!</h1>'

if __name__ == '__main__':
    app.run(debug=True)