## Routes

In this article, we will create some more functions for our flask application.  In Flask, a function represents a `GET` request to a certain path on the server.  This is specified by the [`app.route()` "decorator"](http://flask.pocoo.org/docs/0.12/api/#flask.Flask.route) in the file.  This will help to illustrate on a local level, what happens to HTTP on a global level.  When the `GET` request happens, the function will execute, and return the HTML.

For this exercise, create a copy of the `app.py` file and place it in the same directory, calling it `routes.py`.  Go to the terminal where you were running the previous code, and stop it  if it is still running by pressing `Crtl` + `C`. Run a new command there, which will be the same, except running the code in `routes.py` instead of `app.py`:

        python flask-app/routes.py

As you can see, the argument to the `app.route()` is a string, which contains a file path.  In the first piece of code we used, we specified it was `/`, equivalent to the root of the directory, or the homepage of the Web application.  

Firstly, try and add a new route `/cities` with the function `get_cities()` which returns `"These are the cities"`.  This should be very similar to the code for the previous example, and won't display any cities yet.  We will change this soon!

Add the following text to the `routes.py` file:

    @app.route('/add', methods=['GET', 'POST'])
    def add_city():
        if flask.request.method == 'GET':
            return '<html><body><h1>GET Method</h1><p>Click to post</p><form  action="/add" method="POST"><input type="submit" /></form></body></html>'
        else:
            return '<html><body><h1>Posted successfully!</h1></body></html>'

This route is different, since it accepts either a `GET` or a `POST` request, try and work out what this code is doing.  Then, run it, by restarting the server, and navigating to [http://localhost:5000/add](http://localhost:5000/add).

Another option is to add a parameter to the path, so that the page display is dynamic, depending on the exact path the user enters.  In this function, the page prints the user's username.  The value in `<username>` is passed as a parameter to the function.

    @app.route('/user/<username>')
    def get_user(username):
        return('<h1>Hello, %s</h1>' % username)
        
Try and create a route which takes a parameter `<city>` which prints something like "Welcome to <city name>!"

The main purpose of this section was to introduce you to routes, as an example of how HTTP can work.  We have got the bones of an application here, in the next section we will start adding a bit of functionality to it.  First, to finish, add a couple of routes for `/<city>/<name>/update` and a `/<city>/<name>/delete` route to the file.  Make sure to use the correct method (HTTP verb)!