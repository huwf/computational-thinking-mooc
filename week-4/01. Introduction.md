We introduce the week here, and explain why we are choosing this particular topic, i.e., it is an extension of data representation and abstraction.

The Internet and the Web make use of various network protocols, until we end up with the application that we use every day.

It represents information and display using HTML and CSS, and REST APIs are a clear example of abstraction.

Finally, we will be asking you to make a Web application using Python.  We will provide a lot of code to help you, but you will come to the end having created an application