## Adding some content

Having built the bare bones of our application, we can now start adding some functionality.  We'll start with `cities`.  We can abstract away the database connection for now, so we can get the function to work.  All we want to do is iterate over a list of cities, and display the name of each city.  Modify the `routes.py` function to create a new function `_get_cities_data()`.  This is not a function someone should be able to call from the Web application, but is a function we will use.  

    def _get_cities_data():
        return ['Aberystwyth', 'Brighton', 'Edinburgh']
    
This is a simple list, but our other function doesn't need to know this, it should work the same however.

We can now modify the `cities()` function (for the route `/cities`) to start to return some content.  

    def cities():
        def cities():
            cities = _get_cities_data()
            # For the HTML, we need an unordered list (<ul>)
            # Each item in the list is a list item (<li>) surrounding the text
            cities_list = '<ul>'
            for c in cities:
                cities_list = cities_list + '<li>%s</li>' % c
            cities_list = cities_list + '</ul>'
    
        head =  '<!DOCTYPE html><html><head><title>These are the cities</title></head>'
        body = + '<body><h1>These are the cities</h1>' + cities_list + '</body></html>'
        return head + body

Here we have some placeholder data which we have used to create a list of cities.

The next thing to do, is to create some more placeholder data to include information about the cities, for access on the `/city/name` route.  The information we are going to display about the city is as follows:

    <h2>Welcome to City name</h2>
    <dl>
      <dt>Population</dt>
      <dd>4567</dd>
      <dt>Weather</dt>
      <dd>Cold</dd>
      <dt>Neighbouring cities</dt>
      <dd>City A, City B, ....</dd>
    </dl>
    
Create a function called `_get_city_details(name)`, and use it to display in the  `/city/name` route.  Replace the `City name` text with the value from the `/name` variable.

Now that we have added this information, try and change the list in `cities()` to have a hyperlink to this page.  This will require that you use the anchor tag, and will have to construct the URL using the variable.  If you get stuck, the code to do this is at **LINK**.

In the next step, we will try change this to 






